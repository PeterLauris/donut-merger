﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using TMPro;

[Serializable]
public class UpgradeData
{
    public int FasterSpawningLvl = 0;
    public int FasterRewardsLvl = 0;
    public int BetterDonutsLvl = 0;
    public int TwoDonutsChanceLvl = 0;
    public int BetterDonutsChanceLvl = 0;

    public int DonutAddSprinkleChanceLvl = 0;
    public int MoreEclairsPercentLvl = 0;
    public int SprinklesPerClickLvl = 0;
    public int BoostFromEclairsLvl = 0;
    public int BoostFromSprinklesLvl = 0;
}

public class UpgradeControl : MonoBehaviour
{
    private const int DEFAULT_SPRINKLES_PER_CLICK = 1;
    private const float DEFAULT_DONUT_AWARD_INTERVAL = 2.5f;
    private const float DEFAULT_DONUT_SPAWN_INTERVAL = 3f;
    private const float DEFAULT_BOOST_PER_ECLAIR = 0.05f;

    private const float PER_LEVEL_SPAWNING_TIME_REDUCTION = DEFAULT_DONUT_SPAWN_INTERVAL * 0.01f; //līmenis samazina par 1%
    private const float PER_LEVEL_REWARDS_TIME_REDUCTION = DEFAULT_DONUT_AWARD_INTERVAL * 0.01f; //līmenis samazina par 1%
    private const float PER_LEVEL_TWO_DONUTS_CHANCE = 0.01f;
    private const float PER_LEVEL_BETTER_DONUTS_CHANCE = 0.01f;

    private const float PER_LEVEL_DONUT_ADD_SPRINKLE_CHANCE = 0.005f;
    private const float PER_LEVEL_MORE_ECLAIRS_PERCENT = 0.05f;
    private const float PER_LEVEL_SPRINKLES_CLICK_INCREASE = 1;
    private const float PER_LEVEL_BOOST_FROM_ECLAIRS = 0.01f;
    private const float PER_LEVEL_BOOST_FROM_SPRINKLES = 0.005f;

    #region Pricing
    private const double START_PRICE_FASTER_SPAWNING = 600;
    private const double START_PRICE_FASTER_REWARDS = 300;
    private const double START_PRICE_BETTER_DONUTS = 3000;
    private const double START_PRICE_TWO_DONUTS_CHANCE = 600;
    private const double START_PRICE_BETTER_DONUTS_CHANCE = 300;

    private const double START_PRICE_DONUT_ADD_SPRINKLE_CHANCE = 10;
    private const double START_PRICE_MORE_ECLAIRS_PERCENT = 15;
    private const double START_PRICE_SPRINKLES_PER_CLICK = 25;
    private const double START_PRICE_BOOST_FROM_ECLAIRS = 20;
    private const double START_PRICE_BOOST_FROM_SPRINKLES = 15;

    private const float DONUT_PRICE_GROWTH_COEF = 6.15f;
    private const float SPRINKLE_PRICE_GROWTH_COEF = 2.15f;
    #endregion

    #region UI
    [Header("Faster Spawning")]
    private double       _cachedFasterSpawningPrice;
    public Button               FasterSpawningButton;
    public TextMeshProUGUI      FasterSpawningPriceText;
    [Header("Faster Rewards")]
    private double       _cachedFasterRewardsPrice;
    public Button               FasterRewardsButton;
    public TextMeshProUGUI      FasterRewardsPriceText;
    [Header("Better Donuts")]
    private double       _cachedBetterDonutsPrice;
    public Button               BetterDonutsButton;
    public TextMeshProUGUI      BetterDonutsPriceText;
    [Header("Two Donuts Chance")]
    private double       _cachedTwoDonutsChancePrice;
    public Button               TwoDonutsChanceButton;
    public TextMeshProUGUI      TwoDonutsChancePriceText;
    [Header("Better Donuts Chance")]
    private double       _cachedBetterDonutsChancePrice;
    public Button               BetterDonutsChanceButton;
    public TextMeshProUGUI      BetterDonutsChancePriceText;

    [Header("Donut Spawn Sprinkle Chance")]
    private double       _cachedDonutAddSprinkleChancePrice;
    public Button               DonutAddSprinkleChanceButton;
    public TextMeshProUGUI      DonutAddSprinkleChancePriceText;
    [Header("More Eclairs Percent")]
    private double       _cachedMoreEclairsPercentPrice;
    public Button               MoreEclairsPercentButton;
    public TextMeshProUGUI      MoreEclairsPercentPriceText;
    [Header("Sprinkles Per Click")]
    private double       _cachedSprinklesPerClickPrice;
    public Button               SprinklesPerClickButton;
    public TextMeshProUGUI      SprinklesPerClickPriceText;
    [Header("Boost From Eclairs")]
    private double       _cachedBoostFromEclairsPrice;
    public Button               BoostFromEclairsButton;
    public TextMeshProUGUI      BoostFromEclairsPriceText;
    [Header("Boost From Sprinkles")]
    private double       _cachedBoostFromSprinklesPrice;
    public Button               BoostFromSprinklesButton;
    public TextMeshProUGUI      BoostFromSprinklesPriceText;
    #endregion

    public bool AutoMergeOn = false;

    public enum UpgradeType
    {
        //Donut Upgrades
        FASTER_SPAWNING,
        FASTER_REWARDS,
        BETTER_DONUTS,
        TWO_DONUTS_CHANCE,
        BETTER_DONUTS_CHANCE,

        //Sprinkle Upgrades
        DONUT_ADD_SPRINKLE_CHANCE,
        MORE_ECLAIRS_PERCENT,
        SPRINKLES_PER_CLICK,
        BOOST_FROM_ECLAIRS,
        BOOST_FROM_SPRINKLES
    }

    public static UpgradeControl script;
    public UpgradeData data;

    private void Awake() {
        script = this;
        data = new UpgradeData();
    }

    private void Start() {
        FasterSpawningButton.onClick.AddListener(() => TryPurchaseUpgrade(UpgradeType.FASTER_SPAWNING));
        FasterRewardsButton.onClick.AddListener(() => TryPurchaseUpgrade(UpgradeType.FASTER_REWARDS));
        BetterDonutsButton.onClick.AddListener(() => TryPurchaseUpgrade(UpgradeType.BETTER_DONUTS));
        TwoDonutsChanceButton.onClick.AddListener(() => TryPurchaseUpgrade(UpgradeType.TWO_DONUTS_CHANCE));
        BetterDonutsChanceButton.onClick.AddListener(() => TryPurchaseUpgrade(UpgradeType.BETTER_DONUTS_CHANCE));

        DonutAddSprinkleChanceButton.onClick.AddListener(() => TryPurchaseUpgrade(UpgradeType.DONUT_ADD_SPRINKLE_CHANCE, false));
        MoreEclairsPercentButton.onClick.AddListener(() => TryPurchaseUpgrade(UpgradeType.MORE_ECLAIRS_PERCENT, false));
        SprinklesPerClickButton.onClick.AddListener(() => TryPurchaseUpgrade(UpgradeType.SPRINKLES_PER_CLICK, false));
        BoostFromEclairsButton.onClick.AddListener(() => TryPurchaseUpgrade(UpgradeType.BOOST_FROM_ECLAIRS, false));
        BoostFromSprinklesButton.onClick.AddListener(() => TryPurchaseUpgrade(UpgradeType.BOOST_FROM_SPRINKLES, false));
    }

    public double GetPrice(UpgradeType ut) {
        double startPrice = double.MaxValue;
        int level = 0;
        float priceCoef = DONUT_PRICE_GROWTH_COEF;

        switch (ut) {
            case UpgradeType.FASTER_SPAWNING:
                level = data.FasterSpawningLvl;
                startPrice = START_PRICE_FASTER_SPAWNING;
                break;
            case UpgradeType.FASTER_REWARDS:
                level = data.FasterRewardsLvl;
                startPrice = START_PRICE_FASTER_REWARDS;
                break;
            case UpgradeType.BETTER_DONUTS:
                level = data.BetterDonutsLvl;
                startPrice = START_PRICE_BETTER_DONUTS;
                break;
            case UpgradeType.TWO_DONUTS_CHANCE:
                level = data.TwoDonutsChanceLvl;
                startPrice = START_PRICE_TWO_DONUTS_CHANCE;
                break;
            case UpgradeType.BETTER_DONUTS_CHANCE:
                level = data.BetterDonutsChanceLvl;
                startPrice = START_PRICE_BETTER_DONUTS_CHANCE;
                break;

            case UpgradeType.DONUT_ADD_SPRINKLE_CHANCE:
                level = data.DonutAddSprinkleChanceLvl;
                startPrice = START_PRICE_DONUT_ADD_SPRINKLE_CHANCE;
                priceCoef = SPRINKLE_PRICE_GROWTH_COEF;
                break;
            case UpgradeType.MORE_ECLAIRS_PERCENT:
                level = data.MoreEclairsPercentLvl;
                startPrice = START_PRICE_MORE_ECLAIRS_PERCENT;
                priceCoef = SPRINKLE_PRICE_GROWTH_COEF;
                break;
            case UpgradeType.SPRINKLES_PER_CLICK:
                level = data.SprinklesPerClickLvl;
                startPrice = START_PRICE_SPRINKLES_PER_CLICK;
                priceCoef = SPRINKLE_PRICE_GROWTH_COEF;
                break;
            case UpgradeType.BOOST_FROM_ECLAIRS:
                level = data.BoostFromEclairsLvl;
                startPrice = START_PRICE_BOOST_FROM_ECLAIRS;
                priceCoef = SPRINKLE_PRICE_GROWTH_COEF;
                break;
            case UpgradeType.BOOST_FROM_SPRINKLES:
                level = data.BoostFromSprinklesLvl;
                startPrice = START_PRICE_BOOST_FROM_SPRINKLES;
                priceCoef = SPRINKLE_PRICE_GROWTH_COEF;
                break;
        }

        return Math.Floor(startPrice * Mathf.Pow(priceCoef, level));
    } 

    public bool TryPurchaseUpgrade(UpgradeType ut, bool payWithDonuts = true) {
        double price = GetPrice(ut);
        if (payWithDonuts) {
            if (GameControl.script.data.Donuts >= price) {
                GameControl.script.AddDonuts(-price);
            }
            else return false;
        }
        else {
            if (GameControl.script.data.Sprinkles >= price) {
                GameControl.script.AddSprinkles(-price);
            }
            else return false;
        }

        int level = 0;
        switch (ut) {
            case UpgradeType.FASTER_SPAWNING:
                level = data.FasterSpawningLvl++;
                GameControl.script.RefreshIntervalValues();
                break;
            case UpgradeType.FASTER_REWARDS:
                level = data.FasterRewardsLvl++;
                GameControl.script.RefreshIntervalValues();
                break;
            case UpgradeType.BETTER_DONUTS:
                level = data.BetterDonutsLvl++;
                DonutsControl.script.RefreshDonutElementLevel();
                break;
            case UpgradeType.TWO_DONUTS_CHANCE:
                level = data.TwoDonutsChanceLvl++;
                break;
            case UpgradeType.BETTER_DONUTS_CHANCE:
                level = data.BetterDonutsChanceLvl++;
                break;

            case UpgradeType.DONUT_ADD_SPRINKLE_CHANCE:
                level = data.DonutAddSprinkleChanceLvl++;
                break;
            case UpgradeType.MORE_ECLAIRS_PERCENT:
                level = data.MoreEclairsPercentLvl++;
                break;
            case UpgradeType.SPRINKLES_PER_CLICK:
                level = data.SprinklesPerClickLvl++;
                break;
            case UpgradeType.BOOST_FROM_ECLAIRS:
                level = data.BoostFromEclairsLvl++;
                break;
            case UpgradeType.BOOST_FROM_SPRINKLES:
                level = data.BoostFromSprinklesLvl++;
                break;
        }

        if(!payWithDonuts) {
            GameControl.script.AddSprinkles(0);//lai norefreshotu vajadzīgās vērtības
        }
        DonutsControl.script.RefreshUpgradedValues();
        GameControl.script.RefreshCurrencyTexts();
        RefreshUIPrices(true);

        //AnalyticsEvent.Custom("UpgradePurchased", new Dictionary<string, object>
        //{
        //    { "UpgradeType", (int)ut },
        //    { "UpgradeLevel", level }
        //});
        Firebase.Analytics.Parameter[] LevelUpParameters = {
            new Firebase.Analytics.Parameter("UpgradeType", (int)ut),
            new Firebase.Analytics.Parameter("UpgradeLevel", level)
        };

        return true;
    }

    public void RefreshUIPrices(bool refreshCachedPrices = false) {
        //faster spawning
        if (refreshCachedPrices) _cachedFasterSpawningPrice = GetPrice(UpgradeType.FASTER_SPAWNING);
        FasterSpawningPriceText.text = Utilities.FormattedNumber(_cachedFasterSpawningPrice);
        if (_cachedFasterSpawningPrice <= GameControl.script.data.Donuts) FasterSpawningButton.interactable = true;
        else FasterSpawningButton.interactable = false;

        //faster rewards
        if (refreshCachedPrices) _cachedFasterRewardsPrice = GetPrice(UpgradeType.FASTER_REWARDS);
        FasterRewardsPriceText.text = Utilities.FormattedNumber(_cachedFasterRewardsPrice);
        if (_cachedFasterRewardsPrice <= GameControl.script.data.Donuts) FasterRewardsButton.interactable = true;
        else FasterRewardsButton.interactable = false;

        //better donuts
        if (refreshCachedPrices) _cachedBetterDonutsPrice = GetPrice(UpgradeType.BETTER_DONUTS);
        BetterDonutsPriceText.text = Utilities.FormattedNumber(_cachedBetterDonutsPrice);
        if (_cachedBetterDonutsPrice <= GameControl.script.data.Donuts) BetterDonutsButton.interactable = true;
        else BetterDonutsButton.interactable = false;

        //two donuts chance
        if (refreshCachedPrices) _cachedTwoDonutsChancePrice = GetPrice(UpgradeType.TWO_DONUTS_CHANCE);
        TwoDonutsChancePriceText.text = Utilities.FormattedNumber(_cachedTwoDonutsChancePrice);
        if (_cachedTwoDonutsChancePrice <= GameControl.script.data.Donuts) TwoDonutsChanceButton.interactable = true;
        else TwoDonutsChanceButton.interactable = false;

        //better donuts chance
        if (refreshCachedPrices) _cachedBetterDonutsChancePrice = GetPrice(UpgradeType.BETTER_DONUTS_CHANCE);
        BetterDonutsChancePriceText.text = Utilities.FormattedNumber(_cachedBetterDonutsChancePrice);
        if (_cachedBetterDonutsChancePrice <= GameControl.script.data.Donuts) BetterDonutsChanceButton.interactable = true;
        else BetterDonutsChanceButton.interactable = false;


        //donut add sprinkle chance
        if (refreshCachedPrices) _cachedDonutAddSprinkleChancePrice = GetPrice(UpgradeType.DONUT_ADD_SPRINKLE_CHANCE);
        DonutAddSprinkleChancePriceText.text = Utilities.FormattedNumber(_cachedDonutAddSprinkleChancePrice);
        if (_cachedDonutAddSprinkleChancePrice <= GameControl.script.data.Sprinkles) DonutAddSprinkleChanceButton.interactable = true;
        else DonutAddSprinkleChanceButton.interactable = false;

        //more eclairs percent
        if (refreshCachedPrices) _cachedMoreEclairsPercentPrice = GetPrice(UpgradeType.MORE_ECLAIRS_PERCENT);
        MoreEclairsPercentPriceText.text = Utilities.FormattedNumber(_cachedMoreEclairsPercentPrice);
        if (_cachedMoreEclairsPercentPrice <= GameControl.script.data.Sprinkles) MoreEclairsPercentButton.interactable = true;
        else MoreEclairsPercentButton.interactable = false;

        //sprinkles per click
        if (refreshCachedPrices) _cachedSprinklesPerClickPrice = GetPrice(UpgradeType.SPRINKLES_PER_CLICK);
        SprinklesPerClickPriceText.text = Utilities.FormattedNumber(_cachedSprinklesPerClickPrice);
        if (_cachedSprinklesPerClickPrice <= GameControl.script.data.Sprinkles) SprinklesPerClickButton.interactable = true;
        else SprinklesPerClickButton.interactable = false;

        //boost from eclairs
        if (refreshCachedPrices) _cachedBoostFromEclairsPrice = GetPrice(UpgradeType.BOOST_FROM_ECLAIRS);
        BoostFromEclairsPriceText.text = Utilities.FormattedNumber(_cachedBoostFromEclairsPrice);
        if (_cachedBoostFromEclairsPrice <= GameControl.script.data.Sprinkles) BoostFromEclairsButton.interactable = true;
        else BoostFromEclairsButton.interactable = false;

        //boost from sprinkles
        if (refreshCachedPrices) _cachedBoostFromSprinklesPrice = GetPrice(UpgradeType.BOOST_FROM_SPRINKLES);
        BoostFromSprinklesPriceText.text = Utilities.FormattedNumber(_cachedBoostFromSprinklesPrice);
        if (_cachedBoostFromSprinklesPrice <= GameControl.script.data.Sprinkles) BoostFromSprinklesButton.interactable = true;
        else BoostFromSprinklesButton.interactable = false;
    }

    public float GetAwardInterval() {
        return DEFAULT_DONUT_AWARD_INTERVAL - PER_LEVEL_REWARDS_TIME_REDUCTION * data.FasterRewardsLvl;
    }

    public float GetSpawnInterval() {
        return DEFAULT_DONUT_SPAWN_INTERVAL - PER_LEVEL_SPAWNING_TIME_REDUCTION * data.FasterSpawningLvl;
    }

    public int GetActiveDonutLevel() {
        return 1 + data.BetterDonutsLvl;
    }

    public float GetTwoDonutsChance() {
        return PER_LEVEL_TWO_DONUTS_CHANCE * data.TwoDonutsChanceLvl;
    }

    public float GetBetterDonutsChance() {
        return PER_LEVEL_BETTER_DONUTS_CHANCE * data.BetterDonutsChanceLvl;
    }

    public float GetDonutAddSprinkleChance() {
        return PER_LEVEL_DONUT_ADD_SPRINKLE_CHANCE * data.DonutAddSprinkleChanceLvl;
    }

    public float GetMoreEclairsPercent() {
        return 1 + PER_LEVEL_MORE_ECLAIRS_PERCENT * data.MoreEclairsPercentLvl;
    }

    public float GetSprinklesPerClick() {
        return DEFAULT_SPRINKLES_PER_CLICK + PER_LEVEL_SPRINKLES_CLICK_INCREASE * data.SprinklesPerClickLvl;
    }

    public float GetBoostFromEclairs() {
        return 1 + (float)((DEFAULT_BOOST_PER_ECLAIR + PER_LEVEL_BOOST_FROM_ECLAIRS * data.BoostFromEclairsLvl) * GameControl.script.data.Eclairs);
    }

    public float GetBoostFromEclairs(double addEclairs) {
        return 1 + (float)((DEFAULT_BOOST_PER_ECLAIR + PER_LEVEL_BOOST_FROM_ECLAIRS * data.BoostFromEclairsLvl) * (GameControl.script.data.Eclairs + addEclairs));
    }

    public float GetBoostFromSingleEclair() {
        return DEFAULT_BOOST_PER_ECLAIR + PER_LEVEL_BOOST_FROM_ECLAIRS * data.BoostFromEclairsLvl;
    }

    public float GetBoostFromSprinkles() {
        return 1 + (float)(PER_LEVEL_BOOST_FROM_SPRINKLES * data.BoostFromSprinklesLvl * GameControl.script.data.Sprinkles);
    }
}
