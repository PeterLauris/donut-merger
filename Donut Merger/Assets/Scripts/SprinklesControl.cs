﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprinklesControl : MonoBehaviour
{
    private const int MIN_SPAWN_SECONDS = 20;
    private const int MAX_SPAWN_SECONDS = 35;

    public static SprinklesControl script;

    public Transform SprinkleContainer;
    public GameObject SprinklePrefab;

    private void Awake() {
        script = this;
    }

    private void Start() {
        Invoke("SpawnSprinkle", Utilities.RandomIntFromRange(MIN_SPAWN_SECONDS, MAX_SPAWN_SECONDS));
    }

    public void SpawnSprinkle() {
        GameObject go = Instantiate(SprinklePrefab, SprinkleContainer);

        Vector3 direction = new Vector3((float)Utilities.RandomDoubleFromRange(-1, 1), (float)Utilities.RandomDoubleFromRange(-1, 1), 0);
        direction.Normalize();
        go.GetComponent<SprinkleElement>().Set(direction);

        Invoke("SpawnSprinkle", Utilities.RandomIntFromRange(MIN_SPAWN_SECONDS, MAX_SPAWN_SECONDS));
    }
}
