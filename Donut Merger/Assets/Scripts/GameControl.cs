﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using TMPro;

[Serializable]
public class GameData
{
    public double Donuts;
    public double Sprinkles;
    public double Eclairs;

    public int TopSpawnedDonutLevel;

    public DateTime AutoMergeEndTime;

    public bool NoAdsPurchased;
    public bool IsSoundOn;
    public bool IsMusicOn;

    public int[] AdTypeWeights;

    public GameData()
    {
        Donuts = 0;
        Sprinkles = 0;
        Eclairs = 0;
        IsSoundOn = true;
        IsMusicOn = true;
        TopSpawnedDonutLevel = 1;
        AutoMergeEndTime = DateTime.UtcNow;
        NoAdsPurchased = false;
        AdTypeWeights = new int[] { 5, 5, 5 };
    }
}

public class GameControl : MonoBehaviour
{
    public static bool GAME_LOADED = false;
    public DateTime LoadedSaveTime;

    public static GameControl script;
    public GameData data;

    public static bool IsDraggingDonut = false;

    public System.Random Rnd;
    public Dictionary<int, System.Random> SeededRandoms;

    public TextMeshProUGUI DonutsText;
    public TextMeshProUGUI DPSText;
    public TextMeshProUGUI SprinklesText;

    public Image AwardingLineImage;
    public Image SpawningLineImage;

    private float _donutAwardInterval = 1;
    private float _donutSpawnInterval = 1;
    private float _awardTimerLeft;
    private float _spawnTimerLeft;

    public double EclairMultiplier = 1;
    public double SprinkleMultiplier = 1;

    private bool _hasStartedAwarding = false;
    private bool _shouldSaveGame = false;

    private void Awake() {
        script = this;
        data = new GameData();
        Rnd = new System.Random();
    }

    // Start is called before the first frame update
    void Start() {
        if (SaveLoad.Load()) {
            DonutsControl.script.SetDonutElementsAfterLoad();
            AddDonutsForAwayTime(LoadedSaveTime);

            RefreshCurrencyTexts();
            UpgradeControl.script.RefreshUIPrices();

            DonutsControl.script.RefreshDonutElementLevel();

            SoundsControl.script.ToggleSound(false);
            SoundsControl.script.ToggleMusic(false);

            Debug.Log("Finished loading!");
        }

        HandleAfterLoad();
        _shouldSaveGame = true;

        InvokeRepeating("UpdateDPS", 0.033f, 0.033f);
        InvokeRepeating("UpdateSave", 0.43f, 0.43f);
        InvokeRepeating("UpdateAutoSave", 5.43f, 5.43f);
    }

    void Update() {
        _awardTimerLeft -= Time.deltaTime;
        if (_awardTimerLeft < 0) {
            DonutsControl.script.AwardAllDonuts();
            _awardTimerLeft = _donutAwardInterval;
        }
        
        _spawnTimerLeft -= Time.deltaTime;
        if (_spawnTimerLeft < 0) {
            DonutsControl.script.SpawnNewDonut();

            if (Utilities.RandomDoubleFromRange(0, 1) < UpgradeControl.script.GetTwoDonutsChance()) { //iespējams uzspawno vēl vienu
                DonutsControl.script.SpawnNewDonut();
            }
            _spawnTimerLeft = _donutSpawnInterval;

            if (!_hasStartedAwarding) {
                StartAwarding();
            }
        }

        UpdateLines();
        UpdateDPS();
    }

    public void UpdateSave() {
        if (_shouldSaveGame) {
            SaveGame();
            _shouldSaveGame = false;
        }
    }

    public void UpdateAutoSave() {
        SaveGame();
    }

    public void SaveGame(bool saveImmediately = true) {
        if (!SaveLoad.IsSaving) {
            SaveLoad.Save(saveImmediately);
        }
    }

    void UpdateLines() {
        AwardingLineImage.fillAmount = 1 - _awardTimerLeft / _donutAwardInterval;
        SpawningLineImage.fillAmount = 1 - _spawnTimerLeft / _donutSpawnInterval;
    }

    void UpdateDPS() {
        DPSText.text = string.Format("+{0} DPS", Utilities.FormattedNumber(DonutsControl.script.GetDonutsPerAwarding() / _donutAwardInterval));
    }

    public void RefreshIntervalValues() {
        _donutAwardInterval = UpgradeControl.script.GetAwardInterval();
        _donutSpawnInterval = UpgradeControl.script.GetSpawnInterval();
    }

    public void HandleAfterLoad() {
        _awardTimerLeft = _donutAwardInterval = UpgradeControl.script.GetAwardInterval();
        _spawnTimerLeft = _donutSpawnInterval = UpgradeControl.script.GetSpawnInterval();
        Debug.Log(_donutAwardInterval + " " + _donutSpawnInterval);

        UpgradeControl.script.RefreshUIPrices(true);
        EclairMultiplier = UpgradeControl.script.GetBoostFromEclairs();
        SprinkleMultiplier = UpgradeControl.script.GetBoostFromSprinkles();
        RefreshCurrencyTexts();

        if (DonutsControl.script.GetDonutsPerAwarding() > 0) {
            StartAwarding();
        }
    }

    public void RefreshCurrencyTexts() {
        DonutsText.text = Utilities.FormattedNumber(data.Donuts);
        SprinklesText.text = Utilities.FormattedNumber(data.Sprinkles);
    }

    public void AddDonutsForAwayTime(DateTime saveTime) {
        double totalSeconds = (DateTime.UtcNow - saveTime).TotalSeconds;
        double donutTicks = totalSeconds / _donutAwardInterval;
        double awayDonuts = donutTicks * DonutsControl.script.GetValueOfAllDonuts();
        Debug.Log("Away donuts calculated: " + awayDonuts);
        AddDonuts(awayDonuts);
        PanelControl.script.ShowAwayPanel(awayDonuts);
    }

    public void AddDonuts(double m)  {
        data.Donuts += m;
    }

    public void AddSprinkles(double m)  {
        data.Sprinkles += m;
        SprinkleMultiplier = UpgradeControl.script.GetBoostFromSprinkles();
        DonutsControl.script.RecalculateDonutValues();
        RefreshCurrencyTexts();
    }

    public void AddEclairs(double m) {
        data.Eclairs += m;

        EclairMultiplier = UpgradeControl.script.GetBoostFromEclairs();
    }

    public void ConvertDonutsToEclairs() {
        double eclairs = GetEclairsFromDonuts();
        if (eclairs <= 0) return;

        AddEclairs(eclairs);
        AddDonuts(-data.Donuts);
        AddSprinkles(-data.Sprinkles);

        DonutsControl.script.ResetDonuts();

        _hasStartedAwarding = false;
        HandleAfterLoad();

        //AnalyticsEvent.Custom("ConvertToEclairs", new Dictionary<string, object>
        //{
        //    { "EclairsReceived", eclairs }
        //});
        Firebase.Analytics.FirebaseAnalytics.LogEvent("ConvertToEclairs",
            new Firebase.Analytics.Parameter[] {
                new Firebase.Analytics.Parameter("EclairsReceived", eclairs)
            }
        );
    }

    public bool CanConvertDonutsToEclairs() {
        return GetEclairsFromDonuts() > 0;
    }

    public double GetEclairsFromDonuts() {
        double eclairs = Math.Pow(Math.Log(Math.Sqrt(data.Donuts), 100), 1.7) - 10;
        if (eclairs <= 0) return 0;

        return Math.Floor(eclairs) * UpgradeControl.script.GetMoreEclairsPercent();
    }

    private void StartAwarding() {
        _awardTimerLeft = _donutAwardInterval = UpgradeControl.script.GetAwardInterval();
        UpdateLines();
        AwardingLineImage.gameObject.SetActive(true);
        _hasStartedAwarding = true;
    }

    public void OpenDiscordLink() {
        Application.OpenURL("https://discord.gg/xmkEmCm");
    }
}
