﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DonutImageHandler : MonoBehaviour
{
    public Image DonutImage;

    public void SetDonutLayerImages(int level) {
        DonutImage.sprite = DonutsControl.script.DonutSprites[level-1];
    }
}
