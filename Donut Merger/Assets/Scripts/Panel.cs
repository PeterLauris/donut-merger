﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel : MonoBehaviour
{
    [HideInInspector] public Animator PanelAnimator;

    protected void Awake() {
        PanelAnimator = GetComponent<Animator>();
    }

    public void ClosePanel() {
        PanelAnimator.Play("HidePanel");
    }

    public void AdRefusedClose(bool hardRefuse) {
        AdsControl.script.UserRefusedTheAd(hardRefuse);
        ClosePanel();
    }

    public void OnFinishClosePanel() {
        PanelControl.script.OpenedPanel = null;
        gameObject.SetActive(false);
    }
}
