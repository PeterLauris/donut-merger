﻿using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using System;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Reflection;
using System.Collections.Generic;

using UnityEngine;
using System.Threading;
using FullSerializer;

// === This is the info container class ===
[Serializable()]
public class SaveData : ISerializable
{
    string version;

    // === Values ===
    // Edit these during gameplay
    public GameData gameData;
    public DonutData donutData;
    public UpgradeData upgradeData;
    public DateTime savingTime;

    // === /Values ===

    // The default constructor. Included for when we call it during Save() and Load()
    public SaveData() {
        version = Application.version;
    }

    public SaveData(string _version) {
        version = _version;
    }

    // This constructor is called automatically by the parent class, ISerializable
    // We get to custom-implement the serialization process here
    public SaveData(SerializationInfo info, StreamingContext ctxt) {
        // Get the values from info and assign them to the appropriate properties. Make sure to cast each variable.
        // Do this for each var defined in the Values section above

        //saveGameVersion = (string)info.GetValue("saveGameVersion", typeof(string));
        savingTime = (DateTime)info.GetValue("savingTime", typeof(DateTime));
        gameData = (GameData)info.GetValue("gameData", typeof(GameData));
        donutData = (DonutData)info.GetValue("donutData", typeof(DonutData));
        upgradeData = (UpgradeData)info.GetValue("upgradeData", typeof(UpgradeData));
    }

    // OLD
    // Required by the ISerializable class to be properly serialized. This is called automatically
    public void GetObjectData(SerializationInfo info, StreamingContext ctxt) {
        // Repeat this for each var defined in the Values section
        gameData = GameControl.script.data;
        donutData = DonutsControl.script.data;
        upgradeData = UpgradeControl.script.data;

        PrepareInfo(info);
    }

    public void GetObjectData() {
        // Repeat this for each var defined in the Values section
        savingTime = DateTime.UtcNow;
        gameData = GameControl.script.data;
        donutData = DonutsControl.script.data;
        upgradeData = UpgradeControl.script.data;
    }

    public void PrepareInfo(SerializationInfo info) {
        info.AddValue("savingTime", DateTime.Now);
        info.AddValue("gameData", gameData);
        info.AddValue("donutData", donutData);
        info.AddValue("upgradeData", upgradeData);
    }
}

// === This is the class that will be accessed from scripts ===
public class SaveLoad
{
    static string HASH_DELIMITER = "###";
    public static string hash;
    public static string totalDataString;

    public static bool versionChanged = false;
    public static string oldVersionName = "";

    public static bool IsSaving = false;


    private static readonly fsSerializer _serializer = new fsSerializer();
    static object sync = new object();

    public static void CopyTo(Stream src, Stream dest) {
        byte[] bytes = new byte[4096];

        int cnt;

        while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0) {
            dest.Write(bytes, 0, cnt);
        }
    }


    // Call this to write data
    public static void Save(bool saveImmediately = false)  // Overloaded
    {
        if (!GameControl.GAME_LOADED) {
            return; //if the prefiouse game is not loaded, don't overwrite it
        }

//#if !UNITY_EDITOR
//        bool hasPermission = true;

////#if UNITY_ANDROID
////        if (!UniAndroidPermission.IsPermitted(AndroidPermission.WRITE_EXTERNAL_STORAGE) && !PlayerPrefs.HasKey("storage_permission_denied"))
////        {
////            UniAndroidPermission.RequestPermission(AndroidPermission.WRITE_EXTERNAL_STORAGE, OnAllowWriteExternalStorage, OnDenyWriteExternalStorage, OnDenyAndNeverAskAgainWriteExternalStorage);
////            hasPermission = false;
////        }
////#endif

//        if (hasPermission)
//        {
//            int freeSpace = DiskUtils.CheckAvailableSpace();

//            if (freeSpace < 10)
//            {
//                //PopupControl.script.ShowNotificationTextContainer("Could not save. Not enough space");
//                //return;
//                //LogAnalyticsControl.script.LogEvent(62, freeSpace.ToString());
//            }
//        }
//#endif

        Save(JsonSaveFilePath(), saveImmediately);
    }

    private static SaveData _threadSaveData = null;

    public static void DoSaveThreadWork() {
        try {
            if (_threadSaveData == null) {
                Debug.Log("Thread is null");
                return;
            }

            Debug.Log("Try to save ");

            SaveData data = _threadSaveData;
            _threadSaveData = null;//allow next update

            string serString = Serialize(typeof(SaveData), data);
            hash = Utilities.CalculateMD5Hash(serString);
            serString = hash + HASH_DELIMITER + serString;
            totalDataString = serString;
            try {
                File.WriteAllText(JsonSaveFilePath(), serString);
            }
            catch (Exception e) {
                //TODO: pass to unity
                Debug.Log("[ERROR] " + e);
                //PopupControl.script.AddPopupQueue(PopupControl.popup_types.INFO, GameStrings.Get.warning, GameStrings.Get.could_not_save);
            }
            //CreateSFCreatedFile(GameControl.script.data.saveID);
        }
        catch (Exception e) {
            Debug.Log("[ERROR] " + e);
            //catch all other exceptions so thread wont crash
        }
    }

    public static string Serialize(Type type, object value) {
        fsData data;
        lock (sync) {
            _serializer.TrySerialize(type, value, out data).AssertSuccessWithoutWarnings();
        }
        return fsJsonPrinter.CompressedJson(data);
    }

    public static object Deserialize(Type type, string serializedState) {
        // step 1: parse the JSON data
        fsData data = fsJsonParser.Parse(serializedState);

        // step 2: deserialize the data
        object deserialized = null;
        lock (sync) {
            _serializer.TryDeserialize(data, type, ref deserialized).AssertSuccessWithoutWarnings();
        }

        return deserialized;
    }

    public static void Save(string filePath, bool saveImmediately = false) {
        IsSaving = true;
        
        SaveData data = new SaveData();
        data.GetObjectData();
        //GameControl.script.data.saveID++;

        if (_threadSaveData == null) {
            _threadSaveData = data;
        }

        if (saveImmediately)
            DoSaveThreadWork();

        IsSaving = false;
    }

    // Noskaidro tālāko save versiju, un izsauc tā ielādi
    public static bool Load() {
        SaveData data = new SaveData();
        string allText = "";
        try {
            if (File.Exists(JsonSaveFilePath())) {
                Debug.Log("Load data from: " + JsonSaveFilePath());
                allText = File.ReadAllText(JsonSaveFilePath());
                string[] splitText = allText.Split(new[] { HASH_DELIMITER }, StringSplitOptions.None);

#if !UNITY_EDITOR
                if (splitText[0] != Utilities.CalculateMD5Hash(splitText[1]))
                {
                    throw new Exception("Hash did not match");
                }
#endif
                data = (SaveData)Deserialize(typeof(SaveData), splitText[1]);
                //Debug.Log("done setting data");
            }
        }
        catch (Exception e) {
            Debug.Log("[ERROR] " + e.Message);
            if (File.Exists(JsonSaveFilePath())) {
                Debug.LogWarning("Error loading Json savefile");
                //PopupControl.script.ShowNotificationTextContainer(GameStrings.Get.error_savefile);
                try {
                    System.IO.File.Copy(JsonSaveFilePath(), JsonSaveFilePath(), true);
                }
                catch (Exception e2) {
                    Debug.Log("[ERROR] " + e2.Message);
                }

                //PopupControl.script.AddPopupQueue(PopupControl.popup_types.LOAD_FAILED);
            }
            //ErrorReporter.Report(e, allText);
        }
        return Load(JsonSaveFilePath());
    }

    // Overloaded
    public static bool Load(string filePath) {
        //Debug.Log("####### Start loading ####### " + filePath);
        string allText = "";

        bool wasSaveFileProblem = true; //nav faila vai kāda kļūda lādējot
        try {
            if (File.Exists(JsonSaveFilePath())) {
                SaveData data = new SaveData();
                allText = File.ReadAllText(filePath);
                //string allText = File.ReadAllText(filePath);
                string[] splitText = allText.Split(new[] { HASH_DELIMITER }, StringSplitOptions.None);

#if !UNITY_EDITOR
                if (splitText[0] != Utilities.CalculateMD5Hash(splitText[1]))
                {
                    throw new Exception("Hash did not match");
                }
#endif

                data = (SaveData)Deserialize(typeof(SaveData), splitText[1]);
                //Debug.Log("done setting data");
                if (data.gameData == null) {
                    Debug.LogWarning("gameData is null!!");
                    throw new System.Exception("gameData was not loaded");
                }
                //if (data.gameData.saveGameVersion != GameControl.VERSION_NUMBER) {
                //    versionChanged = true;
                //    oldVersionName = data.gameData.saveGameVersion;
                //    if (PlayerPrefs.GetInt("loadedFromServer") == 0) {
                //        if (Values.GetChangelogs.ContainsKey(GameControl.VERSION_NUMBER) && !EventControl.PlayEvent) {
                //            PopupControl.script.AddPopupQueue(PopupControl.popup_types.CHANGELOG_POPUP, GameControl.VERSION_NUMBER);
                //        }
                //    }
                //}

                GameControl.script.LoadedSaveTime = data.savingTime;

                if (data.gameData != null) {
                    GameControl.script.data = data.gameData;
                }
                if (data.donutData != null) {
                    DonutsControl.script.data = data.donutData;
                }
                if (data.upgradeData != null) {
                    UpgradeControl.script.data = data.upgradeData;
                }


                wasSaveFileProblem = false;
            }
            else {
                Debug.Log("---Savefile doesn't exist!!---");
            }
        }
        catch (Exception e) {
            Debug.LogWarning("[ERROR] " + e.Source + "/" + e.Data + "/" + e.InnerException + "/" + e.Message + "/" + e.StackTrace);
            return false;
        }
        finally {
            Debug.Log("Game Loaded");
            GameControl.GAME_LOADED = true;
            GameControl.script.SaveGame();
        }

        if (wasSaveFileProblem)
            return false;

        return true;
    }

    private static string PersistentPath = Application.persistentDataPath;

    public static string JsonSaveFilePath() {
        return PersistentPath + Path.DirectorySeparatorChar + "dm-savefile.json";
    }

    #region Permission Callbacks
#if UNITY_ANDROID && !UNITY_EDITOR
    private static void OnAllowWriteExternalStorage()
    {
        Save();
    }

    private static void OnDenyWriteExternalStorage()
    {
        Debug.Log("Write permission denied");
    }

    private static void OnDenyAndNeverAskAgainWriteExternalStorage()
    {
        Debug.Log("Write permission denied forever");
        PlayerPrefs.SetInt("storage_permission_denied", 1);
        PlayerPrefs.Save();
    }
#endif
    #endregion  // ~Permission Callbacks
}

public sealed class VersionDeserializationBinder : SerializationBinder
{
    public override Type BindToType(string assemblyName, string typeName) {
        if (!string.IsNullOrEmpty(assemblyName) && !string.IsNullOrEmpty(typeName)) {
            Type typeToDeserialize = null;
            assemblyName = Assembly.GetExecutingAssembly().FullName;
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));
            return typeToDeserialize;
        }
        return null;
    }
}