﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;
using UnityEngine.UI;
using TMPro;

public class AdsControl : MonoBehaviour, IUnityAdsListener
{
    enum AdType {
        AUTO_MERGE,
        FREE_SPRINKLES,
        FREE_DONUTS
    }

    public static AdsControl script;

    private string _androidGameId = "3369639";
    private bool _testMode = false;

    private string _rewardedPlacement = "rewardedVideo";
    private string _bannerPlacement = "bannerAd";

    private const int AUTO_MERGE_REWARD_MINUTES = 3;
    private const int FREE_DONUTS_REWARD_MULTIPLIER = 3;
    private const int FREE_SPRINKLES_REWARD_AMOUNT = 10;

    private const int AD_ALLOW_DELAY_SECONDS = 30;

    private AdType? _nextAdType = null;
    private bool _isRewardedAdReady = false;

    public Sprite AdAutoMergeSprite;
    public Sprite AdFreeDonutsSprite;
    public Sprite AdFreeSprinklesSprite;

    public Animator AdButtonAnimator;

    public Button AdPopupButton, WatchAdButton;

    public TextMeshProUGUI AdDescriptionText;
    public Image AdImage;

    public RectTransform MainGamePanelTransform;

    private DateTime _adAllowTime;
    private int _currentAdRefusalCount = 0;

    void Awake() {
        script = this;
    }

    void Start() {
        InvokeRepeating("UpdateAdButtons", 1.53f, 2.53f);

        WatchAdButton.onClick.AddListener(ShowRewardedAd);

        if (!GameControl.script.data.NoAdsPurchased) {
            Advertisement.AddListener(this);
            Advertisement.Initialize(_androidGameId, _testMode);
            StartCoroutine(ShowBannerWhenReady());
        }

        _adAllowTime = DateTime.UtcNow.AddSeconds(5);
        SelectNextAd();
    }
    
    void UpdateAdButtons() {
        if (_isRewardedAdReady || IsRewardedAdAvailable()) {
            _isRewardedAdReady = true;
        }
        else {
            _isRewardedAdReady = false;
        }
        //AdButtonAnimator.SetBool("IsAdAvailable", _isRewardedAdReady);
        AdPopupButton.interactable = WatchAdButton.interactable = _isRewardedAdReady;
    }

    public void UserRefusedTheAd(bool hardRefusal = false) {
        if (!hardRefusal) {
            _currentAdRefusalCount++;
            if (_currentAdRefusalCount >= 2)
                _currentAdRefusalCount = 0;
            else return;
        }
        Invoke("UpdateRefusedAd", 0.5f);
        _adAllowTime = DateTime.UtcNow.AddSeconds(AD_ALLOW_DELAY_SECONDS);
        _isRewardedAdReady = false;
        UpdateAdButtons();
    }

    public void UpdateRefusedAd() {
        int idx = (int)_nextAdType;
        if (GameControl.script.data.AdTypeWeights[idx] > 1)
            GameControl.script.data.AdTypeWeights[idx]--;

        //AnalyticsEvent.Custom("AdRefused", new Dictionary<string, object>
        //{
        //    { "RewardType", (int)_nextAdType }
        //});
        Firebase.Analytics.FirebaseAnalytics.LogEvent("AdRefused",
            new Firebase.Analytics.Parameter[] {
                new Firebase.Analytics.Parameter("RewardType", (int)_nextAdType)
            }
        );

        SelectNextAd();
    }

    public void SelectNextAd() {
        int totalWeight = GameControl.script.data.AdTypeWeights[0]
            + GameControl.script.data.AdTypeWeights[1]
            + GameControl.script.data.AdTypeWeights[2];
        int typeRand = Utilities.RandomIntFromRange(0, totalWeight-1);
        int selectedIdx = 0;
        for (selectedIdx = 0; selectedIdx < 3; selectedIdx++) {
            typeRand -= GameControl.script.data.AdTypeWeights[selectedIdx];
            if (typeRand <= 0) break;
        }
        _nextAdType = (AdType)selectedIdx;
        _currentAdRefusalCount = 0;
        RefreshAdUI();
    }

    public void RefreshAdUI() {
        string adDescription = "";
        Sprite adSprite = null;

        switch (_nextAdType) {
            case AdType.AUTO_MERGE:
                adDescription = string.Format("Watch an ad to enable Auto-Merge for {0} minutes!", AUTO_MERGE_REWARD_MINUTES);
                adSprite = AdAutoMergeSprite;
                break;
            case AdType.FREE_DONUTS:
                adDescription = string.Format("Watch an ad to receive {0}X Doughnuts!", FREE_DONUTS_REWARD_MULTIPLIER);
                adSprite = AdFreeDonutsSprite;
                break;
            case AdType.FREE_SPRINKLES:
                adDescription = string.Format("Watch an ad to receive {0} Sprinkles!", FREE_SPRINKLES_REWARD_AMOUNT);
                adSprite = AdFreeSprinklesSprite;
                break;
        }
        AdDescriptionText.text = adDescription;
        AdImage.sprite = adSprite;
    }

    public bool IsRewardedAdAvailable() {
        return (_adAllowTime < DateTime.UtcNow) && Advertisement.IsReady(_rewardedPlacement);
    }

    public void ShowRewardedAd() {
        if (GameControl.script.data.NoAdsPurchased) {
            GiveUserReward();
        }
        else if (_isRewardedAdReady) {
            Advertisement.Show(_rewardedPlacement);
            _isRewardedAdReady = false;
        }
    }

    public void GiveUserReward() {
        switch (_nextAdType) {
            case AdType.AUTO_MERGE:
                GameControl.script.data.AutoMergeEndTime = DateTime.UtcNow.AddMinutes(AUTO_MERGE_REWARD_MINUTES);
                _adAllowTime = DateTime.UtcNow.AddMinutes(AUTO_MERGE_REWARD_MINUTES);
                break;
            case AdType.FREE_DONUTS:
                GameControl.script.data.Donuts *= FREE_DONUTS_REWARD_MULTIPLIER;
                _adAllowTime = DateTime.UtcNow.AddSeconds(AD_ALLOW_DELAY_SECONDS);
                break;
            case AdType.FREE_SPRINKLES:
                GameControl.script.data.Sprinkles += FREE_SPRINKLES_REWARD_AMOUNT;
                _adAllowTime = DateTime.UtcNow.AddSeconds(AD_ALLOW_DELAY_SECONDS);
                break;
        }
        GameControl.script.RefreshCurrencyTexts();

        _nextAdType = null;
        SelectNextAd();
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult) {
        if (showResult == ShowResult.Finished) {
            GiveUserReward();
            if (PanelControl.script.OpenedPanel) {
                PanelControl.script.OpenedPanel.ClosePanel();
            }
            //AnalyticsEvent.Custom("AdWatched", new Dictionary<string, object>
            //{
            //    { "RewardType", (int)_nextAdType }
            //});
            Firebase.Analytics.FirebaseAnalytics.LogEvent("AdWatched",
                new Firebase.Analytics.Parameter[] {
                    new Firebase.Analytics.Parameter("RewardType", (int)_nextAdType)
                }
            );
        }
        else if (showResult == ShowResult.Skipped) {
            Debug.Log("Ad was skipped");
        }
        else if (showResult == ShowResult.Failed) {
            Debug.LogWarning("The ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsReady(string placementId) {
        if (placementId == _rewardedPlacement) {
            if (_adAllowTime < DateTime.UtcNow)
                _isRewardedAdReady = true;
        }
    }

    public void OnUnityAdsDidError(string message) {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId) {
        _isRewardedAdReady = false;
    }

    IEnumerator ShowBannerWhenReady() {
        while (!Advertisement.IsReady(_bannerPlacement)) {
            yield return new WaitForSeconds(2.5f);
        }
        Debug.Log("Banner is ready?!");
        if (Advertisement.Banner.isLoaded) {
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
            Advertisement.Banner.Show(_bannerPlacement);

            ReduceGamePanel();
        }
    }

    private void ReduceGamePanel() {
        MainGamePanelTransform.offsetMin = new Vector2(MainGamePanelTransform.offsetMin.x, 52);
    }
}
