﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradePanel : Panel
{
    public Animator UpgradeSwitchingAnimator;

    public Sprite DefaultLongButtonSprite;
    public Sprite ActiveLongButtonSprite;

    public Image DonutsButtonImage;
    public Image SprinklesButtonImage;

    public void OnEnable() {
        if (UpgradeSwitchingAnimator.GetBool("ShowDonuts")) {
            DonutsButtonImage.sprite = ActiveLongButtonSprite;
            SprinklesButtonImage.sprite = DefaultLongButtonSprite;
        }
        else {
            DonutsButtonImage.sprite = DefaultLongButtonSprite;
            SprinklesButtonImage.sprite = ActiveLongButtonSprite;
        }
    }

    public void ShowDonutUpgrades() {
        UpgradeSwitchingAnimator.SetBool("ShowDonuts", true);
        DonutsButtonImage.sprite = ActiveLongButtonSprite;
        SprinklesButtonImage.sprite = DefaultLongButtonSprite;
    }

    public void ShowSprinkleUpgrades() {
        UpgradeSwitchingAnimator.SetBool("ShowDonuts", false);
        DonutsButtonImage.sprite = DefaultLongButtonSprite;
        SprinklesButtonImage.sprite = ActiveLongButtonSprite;
    }
}
