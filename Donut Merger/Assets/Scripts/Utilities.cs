﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

public static class Utilities
{
    public static List<string> NumberAbbreviations = new List<string>() {
        "K", "M", "B", "t", "q", "Q", "s", "S", "o", "n", "d", "U", "D", "T", "Qt", "Qd", "Sd", "St", "O", "N", "v", "c"
    };
    private static int _abbreviationsConstant = (NumberAbbreviations.Count + 1) * 3;

    public static string FormattedNumber(double number) {
        if (number == 0) return "0";

        int digitCount = (int)Math.Floor(Math.Log10(number) + 1);

        if (digitCount < _abbreviationsConstant) {
            if (digitCount < 4) {
                return number.ToString("0");
            }
            else {
                double n = number / Math.Pow(10, digitCount - 1 - (digitCount-1) % 3);
                return string.Format("{0}{1}", n.ToString("0.00"), NumberAbbreviations[(digitCount-1) / 3 - 1]);
            }
        }
        else {
            return number.ToString("0.00E0");
        }
    }

    public static double TimeSpanToSeconds(TimeSpan t) {
        return t.Seconds + t.Minutes * 60 + t.Hours * 60 * 60 + t.Days * 60 * 60 * 24;
    }

    public static string GetTimePassedString(TimeSpan t) {
        return string.Format("{0:0} {1} {2:00}:{3:00}:{4:00}<br>",
            (int)t.TotalDays,
            ((int)t.TotalDays == 1 ? "day" : "days"),
            t.Hours,
            t.Minutes,
            t.Seconds);
    }

    public static string SecondsToString(double s, bool moreHours = false) {
        double d = Math.Ceiling(s);
        TimeSpan ts = TimeSpan.FromSeconds(d);
        if (ts.TotalDays < 1 || moreHours) {
            if (moreHours) {
                return string.Format("{0:00}:{1:00}:{2:00}",
                    (int)ts.TotalHours,
                    ts.Minutes,
                    ts.Seconds);
            }

            return string.Format("{0:00}:{1:00}:{2:00}",
                ts.Hours,
                ts.Minutes,
                ts.Seconds);
        }
        else {
            return string.Format("{0:0} {1} {2:00}:{3:00}:{4:00}",
                (int)ts.TotalDays,
                ((int)ts.TotalDays == 1 ? "day" : "days"),
                ts.Hours,
                ts.Minutes,
                ts.Seconds);
        }

        //return Math.Floor(d/60).ToString("0") + ":" + (d%60).ToString("00");
    }

    public static string SecondsToShortString(double s) {
        double d = Math.Ceiling(s);
        TimeSpan ts = TimeSpan.FromSeconds(d);
        string result = "";

        if (ts.Days >= 1) {
            result += ts.Days + "d ";
            result += ts.Hours + "h ";
        }
        else {
            result += ts.Hours + "h ";
            result += ts.Minutes + "m ";
            result += ts.Seconds + "s";
        }

        return result;

        //return Math.Floor(d/60).ToString("0") + ":" + (d%60).ToString("00");
    }

    public static string NormalizeMinutesString(int minutes) {
        if (minutes <= 60) return minutes + " minutes";
        else if (minutes / 60 <= 24) return (minutes / 60) + " hours";
        else return (minutes / 60 / 24) + " days";
    }

    public static double GetPurchasedUpgradesCount(List<int[]> l) {
        double sum = 0;
        int n = l.Count;
        for (int i = 0; i < n; i++) {
            sum += l[i][1];
        }
        return sum;
    }

    public static int RandomIntFromRange(int minVal, int maxVal, int seed = -1, bool skipCreatedRandoms = false) {
        if (seed > -1) {
            System.Random rnd;
            if (!skipCreatedRandoms && GameControl.script.SeededRandoms.ContainsKey(seed))
                rnd = GameControl.script.SeededRandoms[seed];
            else {
                rnd = new System.Random(seed);
                if (!skipCreatedRandoms)
                    GameControl.script.SeededRandoms.Add(seed, rnd);
            }
            return (int)Math.Floor(rnd.NextDouble() * (maxVal - minVal + 1) + minVal);
        }
        return (int)Math.Floor(GameControl.script.Rnd.NextDouble() * (maxVal - minVal + 1) + minVal);
    }

    //TODO arī saglabāt seedu
    public static double RandomDoubleFromRange(double minVal, double maxVal, int seed = -1) {
        if (seed > -1) {
            System.Random rnd = new System.Random(seed);
            return rnd.NextDouble() * (maxVal - minVal) + minVal;
        }
        return GameControl.script.Rnd.NextDouble() * (maxVal - minVal) + minVal;
    }

    public static Stream GenerateStreamFromString(string s) {
        MemoryStream stream = new MemoryStream();
        StreamWriter writer = new StreamWriter(stream);
        writer.Write(s);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }

    public static string CalculateMD5Hash(string input) {
        MD5 md5 = System.Security.Cryptography.MD5.Create();

        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input + "PETERIS");
        byte[] hash = md5.ComputeHash(inputBytes);

        // step 2, convert byte array to hex string
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.Length; i++) {
            sb.Append(hash[i].ToString("X2"));
        }

        return sb.ToString();

    }

    public static void Shuffle<T>(this IList<T> list) {
        int n = list.Count;
        while (n > 1) {
            n--;
            int k = GameControl.script.Rnd.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static int GetIntFromMID(string mid) {
        int val = 0;

        for (int i = 0; i < mid.Length; i++) {
            if (mid[i] >= '0' && mid[i] <= '9') {
                val += (int)mid[i]; //char kodi, lai rezultats atskirtos ari, ja dazads ciparu skaits
            }
        }

        Debug.Log("DeviceInt: " + val);

        return val;
    }

    public static int GetCharSumFromString(string s) {
        int val = 0;

        for (int i = 0; i < s.Length; i++) {
            val += s[i]; //char kodi, lai rezultats atskirtos ari, ja dazads ciparu skaits
        }

        return val;
    }

    public static string CapitalizeFirstLetter(this string s) {
        if (String.IsNullOrEmpty(s)) return s;
        if (s.Length == 1) return s.ToUpper();
        return s.Remove(1).ToUpper() + s.Substring(1);
    }

    public static DateTime FromUNIXTimestamp(double timestamp) {
        return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(timestamp);
    }

    public static DateTime FromLocalTimestamp(double timestamp) {
        return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local).AddSeconds(timestamp);
    }

    public static void ClearConsole() {
#if UNITY_EDITOR
        var logEntries = System.Type.GetType("UnityEditor.LogEntries, UnityEditor.dll");

        var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);

        clearMethod.Invoke(null, null);
#endif
    }
}
