﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelControl : MonoBehaviour
{
    public static PanelControl script;

    public AwayPanel AwayPanel;

    public Panel OpenedPanel;

    private void Awake() {
        script = this;
    }

    public void ShowPanel(GameObject go) {
        Panel p = go.GetComponent<Panel>();
        if (p != null) {
            go.SetActive(true);
            OpenedPanel = p;
            //p.PanelAnimator.Play("ShowPanel");
        }
    }

    public void ShowAwayPanel(double donuts) {
        AwayPanel.DonutsEarnedText.text = string.Format("While you were away, you received {0} Doughnuts!", Utilities.FormattedNumber(donuts));
        ShowPanel(AwayPanel.gameObject);
    }

    public void ShowEclairsPanel(EclairsPanel ep) {
        double eclairCount = GameControl.script.GetEclairsFromDonuts();

        if (eclairCount == 0) {
            ep.EclairAmountDescriptionText.text = "You don't have enough Doughnuts to exchange!";
            ep.ConvertButton.interactable = false;
        }
        else {
            ep.EclairAmountDescriptionText.text = string.Format("Exchange Doughnuts into {0} Eclairs!", eclairCount);
            ep.ConvertButton.interactable = true;
        }
        ep.EclairBoostDescriptionText.text = string.Format("Each Eclair will increase the Doughnut output by {0}%!", Math.Round(UpgradeControl.script.GetBoostFromSingleEclair()*100));
        ep.CurrentBoostText.text = string.Format("+{0}%", Math.Round((UpgradeControl.script.GetBoostFromEclairs()-1)*100));
        ep.AfterBoostText.text = string.Format("+{0}%", Math.Round((UpgradeControl.script.GetBoostFromEclairs(eclairCount)-1)*100));

        ShowPanel(ep.gameObject);
    }
}
