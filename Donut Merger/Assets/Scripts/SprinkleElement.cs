﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprinkleElement : MonoBehaviour
{
    private const float SPEED = 50f;
    private const float ROTATION_SPEED = 10f;

    public Vector3 Direction;
    public Transform SprinkleTransform;
    public Transform ParticleTransform;

    void Start()  {
        Invoke("DestroySprinkle", 30f);
    }

    // Update is called once per frame
    void Update() {
        transform.localPosition += Direction * Time.deltaTime * SPEED;
        SprinkleTransform.eulerAngles += new Vector3(0, 0, ROTATION_SPEED * Time.deltaTime);
    }

    public void Set(Vector3 d) {
        Direction = d;
        transform.localPosition = new Vector3(-Direction.x * Screen.width / 1.2f, -Direction.y * Screen.height / 1.2f, 0);
        //ParticleTransform.right = Direction.normalized * -1;
    }

    public void Collect() {
        GameControl.script.AddSprinkles(UpgradeControl.script.GetSprinklesPerClick());
        SoundsControl.script.PlaySprinkleSound();
        DestroySprinkle();
    }

    public void DestroySprinkle() {
        Destroy(gameObject);
    }
}
