﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class DonutData
{
    public List<int> SpawnedDonutLevels;

    public DonutData() {
        SpawnedDonutLevels = new List<int>() { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    }
}


public class DonutsControl : MonoBehaviour
{
    public static int DONUT_COUNT = 20;
    private static double MAX_MATCH_DISTANCE = 0.3;

    public static DonutsControl script;
    public DonutData data;

    public Transform DonutContainer;
    public GameObject DonutPrefab;
    public List<Sprite> DonutSprites;
    //public List<Sprite> DonutBaseSprites;
    //public List<Sprite> DonutCircleFrostingSprites;
    //public List<Sprite> DonutCircleToppingSprites;

    [HideInInspector] public GameObject DonutDragObject;
    [HideInInspector] public DonutImageHandler DonutDragImageHandler;
    [HideInInspector] public DonutImageHandler DonutAnimationImageHandler;

    private List<DonutElement> _donutElements;

    public List<Color> DonutColors;


    #region Moving Animations
    private const float MOVING_ANIMATION_TIME = 0.1f;
    private bool _movingAnimationActive = false;
    private float _movingAnimationTimePassed = MOVING_ANIMATION_TIME;
    private Vector3 _movingStartPosition;
    private Vector3 _movingEndPosition;
    private DonutElement _movedElement;

    private bool _mergeOnEnd = false;
    private DonutElement _autoMergeTargetElement;
    #endregion

    //upgraded values (refreshed on start)
    private int _activeDonutLevel = 1;
    public int ActiveDonutLevel {
        get {
            return _activeDonutLevel;
        }
    }

    private void Awake() {
        script = this;
        data = new DonutData();

        _donutElements = new List<DonutElement>();
    }

    // Start is called before the first frame update
    void Start() {
        SpawnDonutElements();

        RefreshUpgradedValues();
        InvokeRepeating("UpdateAutoMerging", 0.53f, 0.53f);
        InvokeRepeating("UpdateDonutLevelData", 2.2f, 2.2f);
    }
    
    void Update() {
        if (_movingAnimationActive) {
            if (_movingAnimationTimePassed <= MOVING_ANIMATION_TIME) {
                _movingAnimationTimePassed += Time.deltaTime;
                DonutAnimationImageHandler.transform.position = Vector3.Lerp(_movingStartPosition, _movingEndPosition, _movingAnimationTimePassed / MOVING_ANIMATION_TIME);
            }
            else {
                _movingAnimationActive = false;
                DonutAnimationImageHandler.gameObject.SetActive(false);

                if (_mergeOnEnd) {
                    MergeDonuts(_movedElement, _autoMergeTargetElement);
                }
                else {
                    _movedElement.HandleDragEnd();
                }
            }
        }
    }

    public void UpdateAutoMerging() {
        if (GameControl.script.data.AutoMergeEndTime > DateTime.UtcNow) {
            for (int i = 0; i < _donutElements.Count; i++) {
                for (int j = _donutElements.Count-1; j > i; j--) {
                    if (!_donutElements[i].IsEmpty() && _donutElements[i].DonutLevel == _donutElements[j].DonutLevel) {
                        _autoMergeTargetElement = _donutElements[i];
                        Debug.Log("Merge levels: " + _donutElements[j].DonutLevel);
                        StartMovingAnimation(_donutElements[j].DonutInactiveImage.transform.position, _donutElements[i].DonutInactiveImage.transform.position, _donutElements[j], true);
                    }
                }
            }
        }
    }

    public void UpdateDonutLevelData() {
        if (GameControl.GAME_LOADED) {
            for (int i = 0; i < DONUT_COUNT; i++) {
                data.SpawnedDonutLevels[i] = _donutElements[i].DonutLevel;
            }
        }
    }

    public void StartMovingAnimation(Vector3 startPos, Vector3 endPos, DonutElement de, bool mergeOnEnd = false) {
        if (_movingAnimationActive && _movedElement != null) {
            _movedElement.HandleDragEnd();
        }

        _movedElement = de;
        _movedElement.DonutImageContainer.SetActive(false);

        if (mergeOnEnd) {
            _movedElement.DonutInactiveImage.gameObject.SetActive(true);
        }

        _movingStartPosition = startPos;
        _movingEndPosition = endPos;

        DonutAnimationImageHandler.SetDonutLayerImages(de.DonutLevel);
        DonutAnimationImageHandler.transform.position = _movingStartPosition;

        _movingAnimationTimePassed = 0;
        _movingAnimationActive = true;

        _mergeOnEnd = mergeOnEnd;

        DonutDragImageHandler.gameObject.SetActive(false);
        DonutAnimationImageHandler.gameObject.SetActive(true);
    }

    public void RefreshUpgradedValues() {
        _activeDonutLevel = UpgradeControl.script.GetActiveDonutLevel();
        //Debug.Log(string.Format("Upgraded values: {0} {1} {2}", _donutAwardInterval, _donutSpawnInterval, _activeDonutLevel));
    }

    public void AwardAllDonuts() {
        for (int i = 0; i < DONUT_COUNT; i++) {
            _donutElements[i].Award();
        }
        GameControl.script.RefreshCurrencyTexts();
        UpgradeControl.script.RefreshUIPrices();
    }

    public double GetValueOfAllDonuts() {
        double value = 0;
        for (int i = 0; i < DONUT_COUNT; i++) {
            value += _donutElements[i].CalculatedValue;
        }
        return value;
    }

    public void SpawnNewDonut() {
        //Debug.Log("Spawn new");
        for (int i = 0; i < DONUT_COUNT; i++) {
            if (_donutElements[i].IsEmpty()) {
                _donutElements[i].SpawnActiveDonut(true, Utilities.RandomDoubleFromRange(0, 1) < UpgradeControl.script.GetBetterDonutsChance());
                return;
            }
        }
    }

    private void SpawnDonutElements()
    {
        for (int i = 0; i < DONUT_COUNT; i++)
        {
            GameObject go = Instantiate(DonutPrefab, DonutContainer);
            go.name = "Donut_" + (i + 1);
            _donutElements.Add(go.GetComponent<DonutElement>());

            if (i == DONUT_COUNT-1 && DonutDragImageHandler == null) {
                go.SetActive(false);
                GameObject dieGo = Instantiate(_donutElements[0].DonutImageContainer, DonutContainer);
                DonutDragImageHandler = dieGo.GetComponent<DonutImageHandler>();
                DonutDragImageHandler.gameObject.SetActive(true);
                DonutContainer.GetComponent<CanvasGroup>().alpha = 0;
                Invoke("FixDragImage", 0.1f);
            }
        }

        //DONUT_WIDTH = _donutElements[0].DonutImage.sprite.rect.width;
        //Debug.Log("Donut width: " + DONUT_WIDTH);
    }

    public void SetDonutElementsAfterLoad() {
        for (int i = 0; i < DONUT_COUNT; i++) {
            if (data.SpawnedDonutLevels[i] > 0)
                _donutElements[i].SetDonut(data.SpawnedDonutLevels[i]);
        }

        //DONUT_WIDTH = _donutElements[0].DonutImage.sprite.rect.width;
        //Debug.Log("Donut width: " + DONUT_WIDTH);
    }

    public void RefreshDonutElementLevel() {
        _activeDonutLevel = UpgradeControl.script.GetActiveDonutLevel();

        for (int i = 0; i < _donutElements.Count; i++) {
            if (!_donutElements[i].IsEmpty() && _donutElements[i].DonutLevel < _activeDonutLevel) {
                _donutElements[i].SpawnActiveDonut(false);
            }
        }
    }

    public void ResetDonuts() {
        _activeDonutLevel = 1;

        for (int i = 0; i < _donutElements.Count; i++) {
            _donutElements[i].RemoveDonut();
        }
    }

    private void FixDragImage() {
        DonutDragImageHandler.gameObject.AddComponent<LayoutElement>().ignoreLayout = true;
        DonutDragImageHandler.gameObject.SetActive(false);
        DonutAnimationImageHandler = Instantiate(DonutDragImageHandler, DonutDragImageHandler.gameObject.transform.parent);
        DonutAnimationImageHandler.GetComponent<RectTransform>().anchoredPosition = DonutDragImageHandler.GetComponent<RectTransform>().anchoredPosition;
        DonutAnimationImageHandler.GetComponent<RectTransform>().sizeDelta = DonutDragImageHandler.GetComponent<RectTransform>().sizeDelta;
        _donutElements[_donutElements.Count - 1].gameObject.SetActive(true);
        DonutContainer.GetComponent<CanvasGroup>().alpha = 1;
    }

    public void RecalculateDonutValues() {
        for (int i = 0; i < DONUT_COUNT; i++) {
            if (!_donutElements[i].IsEmpty()) {
                _donutElements[i].CalculateDonutValue(_donutElements[i].DonutLevel);
            }
        }
    }

    public double GetDonutsPerAwarding() {
        double res = 0;
        for (int i = 0; i < DONUT_COUNT; i++) {
            if (!_donutElements[i].IsEmpty()) {
                res += _donutElements[i].CalculatedValue;
            }
        }
        return res;
    }

    public bool TryMergeOnRelease(DonutElement de) {
        DonutElement closestElement = null;
        double closestDistance = MAX_MATCH_DISTANCE;

        for (int i = 0; i < _donutElements.Count; i++) {
            if (de == _donutElements[i]) continue;

            Vector3 posD1 = de.DonutImageContainer.transform.position;
            Vector3 posD2 = _donutElements[i].DonutImageContainer.transform.position;
            double distanceSquared = Math.Pow(posD1.x - posD2.x, 2) + Math.Pow(posD1.y - posD2.y, 2); //sqrt var nevilkt, jo tikai salidzina
            //Debug.Log(_donutElements[i] + " distance: " + distanceSquared);

            if (distanceSquared <= closestDistance) {
                closestDistance = distanceSquared;
                closestElement = _donutElements[i];
            }
        }

        if (closestElement != null) {
            if (closestElement.IsEmpty()) { //pārliek otrā slotā
                closestElement.SetDonut(de, true);
                de.RemoveDonut();

                StartMovingAnimation(DonutDragImageHandler.transform.position, closestElement.DonutInactiveImage.transform.position, closestElement);
                return false;
            }
            else {
                //Debug.Log("Closest element: " + closestElement.name + " (" + closestDistance + ")");

                if (closestElement.DonutLevel == de.DonutLevel) {
                    if (closestElement.IncreaseDonutNumber()) {
                        de.RemoveDonut();
                        closestElement.PlayMergeAnimation();
                        SoundsControl.script.PlayMergeSound();
                        return true;
                    }
                }
            }
        }

        StartMovingAnimation(DonutDragImageHandler.transform.position, de.DonutInactiveImage.transform.position, de);
        return false;
    }

    public void MergeDonuts(DonutElement source, DonutElement target) {
        if (source.DonutLevel == target.DonutLevel) {
            if (target.IncreaseDonutNumber()) {
                source.RemoveDonut();
                target.PlayMergeAnimation();
                SoundsControl.script.PlayMergeSound();
                //return true;
            }
        }
        //return false;
    }
}
