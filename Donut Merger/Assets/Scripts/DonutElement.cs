﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using TMPro;
using System;

public class DonutElement : MonoBehaviour
{
    public TextMeshProUGUI MoneyText;
    public Animator FadeTextAnimator;

    public Image DonutInactiveImage;
    public GameObject DonutImageContainer;
    public DonutImageHandler DonutImage;

    private Animator _donutAnimator;

    private int _donutLevel = 0;
    public int DonutLevel {
        get {
            return _donutLevel;
        }
    }

    private double _calculatedValue = 0;
    public double CalculatedValue {
        get {
            return _calculatedValue;
        }
    }

    private Vector3 screenPoint;
    private Vector3 offset;

    private void Awake() {
        _donutAnimator = GetComponent<Animator>();
    }

    public void SpawnActiveDonut(bool playAnimation = true, bool spawnBetter = false)
    {
        _donutLevel = DonutsControl.script.ActiveDonutLevel + (spawnBetter ? 1 : 0);

        SetDonutInfo();

        if (_donutLevel > GameControl.script.data.TopSpawnedDonutLevel) {
            GameControl.script.data.TopSpawnedDonutLevel = _donutLevel;
            DonutListControl.script.UpdateCollectedListDonuts();
        }

        if (Utilities.RandomDoubleFromRange(0, 1) < UpgradeControl.script.GetDonutAddSprinkleChance()) {
            GameControl.script.AddSprinkles(1);
        }

        if (playAnimation) {
            _donutAnimator.Play("DonutSpawn");
        }
    }

    public void CalculateDonutValue(int nr) {
        _calculatedValue = Math.Pow(3, nr - 1) * GameControl.script.EclairMultiplier * GameControl.script.SprinkleMultiplier;
    }

    public void SetDonutInfo(bool isMoving = false, int level = -1) {
        if (level == -1) level = _donutLevel;

        CalculateDonutValue(level);
        
        DonutImage.SetDonutLayerImages(level);
        DonutImageContainer.SetActive(true);
        if (!isMoving) {
            DonutInactiveImage.gameObject.SetActive(false);
        }
    }

    public void RemoveDonut() {
        _donutLevel = 0;
        _calculatedValue = 0;
        DonutInactiveImage.gameObject.SetActive(true);
        DonutImageContainer.transform.localPosition = Vector2.zero;
        DonutImageContainer.SetActive(false);
    }

    public void Award() {
        if (_donutLevel > 0) {
            GameControl.script.AddDonuts(_calculatedValue);
            ShowMoneyFadeText(_calculatedValue);
        }
    }

    public void ShowMoneyFadeText(double d)
    {
        MoneyText.text = string.Format("+{0}", Utilities.FormattedNumber(d));
        FadeTextAnimator.Play("FadeText");
    }

    public bool IsEmpty()
    {
        return _donutLevel == 0;
    }

    public bool IncreaseDonutNumber() {
        if (_donutLevel >= DonutsControl.script.DonutSprites.Count) return false; //MAX donut reached
        _donutLevel++;
        SetDonutInfo();

        if (_donutLevel > GameControl.script.data.TopSpawnedDonutLevel) {
            GameControl.script.data.TopSpawnedDonutLevel = _donutLevel;
            DonutListControl.script.UpdateCollectedListDonuts();

            //AnalyticsEvent.Custom("NewLevelReached", new Dictionary<string, object>
            //{
            //    { "DonutLevel", _donutLevel }
            //});
            Firebase.Analytics.FirebaseAnalytics.LogEvent("NewLevelReached",
                new Firebase.Analytics.Parameter[] {
                    new Firebase.Analytics.Parameter("DonutLevel", _donutLevel)
                }
            );
        }
        return true;
    }

    public void SetDonut(DonutElement de, bool isMoving = false) {
        _donutLevel = de.DonutLevel;
        SetDonutInfo(isMoving);
    }

    public void SetDonut(int level) {
        _donutLevel = level;
        SetDonutInfo();
    }

    public void OnPointerDown() {
        if (GameControl.IsDraggingDonut) return;
        if (_donutLevel <= 0) return;

        GameControl.IsDraggingDonut = false;
        
        DonutInactiveImage.gameObject.SetActive(true);
        screenPoint = Camera.main.WorldToScreenPoint(DonutImageContainer.transform.position);
        offset = DonutImageContainer.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        
        DonutsControl.script.DonutDragImageHandler.SetDonutLayerImages(_donutLevel);
        DonutsControl.script.DonutDragImageHandler.gameObject.SetActive(true);
        DonutsControl.script.DonutDragImageHandler.transform.position = DonutImageContainer.transform.position;
    }

    public void OnDrag()  {
        //Debug.Log("OnDrag");
        Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
        DonutImageContainer.transform.position = cursorPosition;
        DonutsControl.script.DonutDragImageHandler.transform.position = cursorPosition;
    }

    public void OnPointerUp() {
        if (DonutsControl.script.TryMergeOnRelease(this)) {
            //DonutImage.gameObject.SetActive(false);
            //HandleDragEnd();
            HandleDragEnd();
        }
        GameControl.IsDraggingDonut = false;
    }

    public void HandleDragEnd() {
        bool isEmpty = IsEmpty();
        DonutInactiveImage.gameObject.SetActive(isEmpty);
        DonutImageContainer.gameObject.SetActive(!isEmpty);
        DonutImageContainer.transform.localPosition = Vector2.zero;
        DonutsControl.script.DonutDragImageHandler.gameObject.SetActive(false);
    }

    public void PlayMergeAnimation() {
        _donutAnimator.Play("DonutMerge");
    }
}
