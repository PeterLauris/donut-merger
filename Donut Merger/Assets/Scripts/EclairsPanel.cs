﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EclairsPanel : Panel
{
    public TextMeshProUGUI EclairAmountDescriptionText;
    public TextMeshProUGUI EclairBoostDescriptionText;
    public TextMeshProUGUI CurrentBoostText;
    public TextMeshProUGUI AfterBoostText;
    public Button ConvertButton;
}
