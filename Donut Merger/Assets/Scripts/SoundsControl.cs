﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SoundsControl : MonoBehaviour
{
    public static SoundsControl script;

    public TextMeshProUGUI SettingsSoundText;
    public TextMeshProUGUI SettingsMusicText;

    public AudioSource MusicAudioSource;
    public AudioSource SoundsAudioSource;

    public AudioClip MergeClip;
    public AudioClip SprinkleClip;
    public AudioClip ClickClip;

    private void Awake() {
        script = this;
    }

    public void ToggleSound(bool toggle = true) {
        if (toggle) {
            GameControl.script.data.IsSoundOn = !GameControl.script.data.IsSoundOn;
        }
        SoundsAudioSource.mute = !GameControl.script.data.IsSoundOn;
        SettingsSoundText.text = "Sound: " + (GameControl.script.data.IsSoundOn ? "ON" : "OFF");
    }

    public void ToggleMusic(bool toggle = true) {
        if (toggle) {
            GameControl.script.data.IsMusicOn = !GameControl.script.data.IsMusicOn;
        }
        MusicAudioSource.mute = !GameControl.script.data.IsMusicOn;
        SettingsMusicText.text = "Music: " + (GameControl.script.data.IsMusicOn ? "ON" : "OFF");
    }

    public void PlayMergeSound() {
        PlaySound(MergeClip);
    }

    public void PlaySprinkleSound() {
        PlaySound(SprinkleClip);
    }

    public void PlayClickSound() {
        PlaySound(ClickClip);
    }

    public void PlaySound(AudioClip clip) {
        SoundsAudioSource.clip = clip;
        SoundsAudioSource.Play();
    }
}
