﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DonutListControl : MonoBehaviour
{
    public static DonutListControl script;

    public Transform DonutContainer;
    public GameObject DonutElementPrefab;

    public List<DonutElement> SpawnedDonuts;

    private int _lastUpdatedLevel = 0;

    private void Awake() {
        script = this;
    }

    private void Start() {
        SpawnDonutListElements();
        UpdateCollectedListDonuts();
    }

    private void SpawnDonutListElements() {
        for (int i = 0; i < DonutsControl.script.DonutSprites.Count; i++) {
            GameObject go = Instantiate(DonutElementPrefab, DonutContainer);
            DonutElement de = go.GetComponent<DonutElement>();
            de.SetDonutInfo(false, i + 1);
            de.DonutInactiveImage.gameObject.SetActive(true);
            de.DonutImageContainer.SetActive(false);
            de.DonutImageContainer.GetComponent<EventTrigger>().enabled = false;
            SpawnedDonuts.Add(de);
        }
    }

    public void UpdateCollectedListDonuts() {
        for (int i = _lastUpdatedLevel; i < SpawnedDonuts.Count; i++) {
            if (i + 1 <= GameControl.script.data.TopSpawnedDonutLevel) {
                _lastUpdatedLevel = i + 1;
                SpawnedDonuts[i].DonutInactiveImage.gameObject.SetActive(false);
                SpawnedDonuts[i].DonutImageContainer.SetActive(true);
            }
            else break;
        }
    }
}
